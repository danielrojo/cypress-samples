    Scenario: Login process with valid credentials as a standard user
        Given I am on the login page and I am a standard user
        When I enter valid credentials
        Then I should be logged in as a standard user

    Scenario: Login process with invalid credentials as a standard user
        Given I am on the login page and I am a standard user
        When I enter invalid credentials
        Then I should see an error message

    Scenario: Login process with valid credentials as a locked_out_user
        Given the user is on the login page
        When the user enters valid credentials as a locked_out_user
        Then the user should see an error message indicating that the user has been locked out

    Scenario: Login process with valid credentials as a problem_user
        Given the user is on the login page
        When the user enters valid credentials as a problem_user
        Then the user should see an error message indicating that there is a problem with the login credentials that were provided

    Scenario: Login process with valid credentials as a performance_glitch_user
        Given the user is on the login page
        When the user enters valid credentials as a performance_glitch_user
        Then the user should be logged in as a performance_glitch_user, with substantial delay in some actions

    Scenario: adding a product to the empty cart as a standard user
        Given The user is on the products page, successfully logged in as a standard user, and there are no items in the cart
        When The user clicks "add to cart" button for a product
        Then The product should be add to the cart
        And The user should see the number 1 of items in the cart icon

    Scenario: adding a product to the non empty cart as a standard user
        Given The user is on the products page, successfully logged in as a standard user, and there are some items in the cart
        When The user clicks "add to cart" button for a product
        Then The product should be add to the cart
        And The user should see the number of items in the cart icon increased by 1

    Scenario: sorting the products by name (descending) 
        Given a default order for the products
        When I select "Name Z to A"
        Then the products should be sorted by name (descending)
    
    Scenario: sorting the products by name (ascending) 
        Given a default order for the products
        When I select "Name A to Z"
        Then the products should be sorted by name (ascending)
    
    Scenario: sorting the products by price (low to high) 
        Given a default order for the products
        When I select "Price (low to high)"
        Then the products should be sorted by price (low to high)
    
    Scenario: sorting the products by price (high to low) 
        Given a default order for the products
        When I select "Price (high to low)"
        Then the products should be sorted by price (high to low)

    Scenario: removing a product from cart with just 1 product 
        Given The user is on the products page, successfully logged in as a standard user, and there is 1 item in the cart
        When The user clicks "remove" button for a product
        Then The product should be removed from the cart
        And The user should see the cart icon as empty

    Scenario: removing a product from cart with more than 1 product 
        Given The user is on the products page, successfully logged in as a standard user, and there are more than 1 items in the cart
        When The user clicks "remove" button for a product
        Then The product should be removed from the cart
        And The user should see the number of items in the cart icon decreased by 1

    Scenario: browse to cart page When cart is empty
        Given The user is on the products page, successfully logged in as a standard user, and there are some items in the cart 
        When The user clicks the cart icon
        Then The user should be redirected to the cart page
        And The user should not see any products in the cart
        And Checkout button should be disabled
    
    Scenario: browse to cart page When cart is not empty
        Given The user is on the products page, successfully logged in as a standard user, and there are some items in the cart 
        When The user clicks the cart icon
        Then The user should be redirected to the cart page
        And The user should see the products in the cart
        And Checkout button should be enabled
    
    Scenario: Remove an item from the cart When there is only one item in the cart
        Given The user is on the cart page, successfully logged in as a standard user, and there is 1 item in the cart 
        When The user clicks the remove button for the product
        Then The product should be removed from the cart
        And The user should see the Checkout button disabled

    Scenario: Remove an item from the cart When there are more than one item in the cart
        Given The user is on the cart page, successfully logged in as a standard user, and there are more than 1 items in the cart 
        When The user clicks the remove button for the product
        Then The product should be removed from the cart
        And The user should see the other products in the cart
        And The user should see the Checkout button enabled

    Scenario: The user is logged in and logs out
        Given The user is logged in as a standard user
        When The user clicks the logout button in side menu
        Then The user should be redirected to the login page
        And The user should see an empty login form
