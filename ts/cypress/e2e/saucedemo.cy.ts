describe('SauceDemo', () => {

    let loggedIn = false;


    it('Login process with valid credentials as a standard user', () => {
        loggedIn = logIn();
        cy.get('.app_logo').should('be.visible').should('have.text', 'Swag Labs');
        
    });

    it('Login process with invalid credentials as a standard user', () => {
        loggedIn = logIn('standard_user', 'un_password');
        cy.contains('Epic sadface: Username and password do not match any user in this service')
    });

    it ('Login process with valid credentials as a locked_out_user', () => {
        loggedIn = logIn('locked_out_user');
        cy.contains('Epic sadface: Sorry, this user has been locked out')
    });

    it ('Login process with valid credentials as a problem_user', () => {
        loggedIn = logIn('problem_user');
        cy.get('.app_logo').should('be.visible').should('have.text', 'Swag Labs');
        // check src from image
        cy.get('img.inventory_item_img').should('have.attr', 'src').should('include', 'static/media/sl-404.168b1cce.jpg');

    });

    it ('Login process with valid credentials as a performance_glitch_user', () => {
        loggedIn = logIn('performance_glitch_user');
    });

    function logIn(user = 'standard_user', password='secret_sauce'): boolean {
        cy.visit('https://www.saucedemo.com');
        cy.get('[data-test=username]').click();
        cy.get('[data-test=username]').type(user);
        cy.get('[data-test=password]').click();
        cy.get('[data-test=password]').type(password);
        cy.get('[data-test=login-button]').click();
        return true;
    }

    function logOut() {
        // cy.visit('https://www.saucedemo.com/inventory.html');
        cy.get('#react-burger-menu-btn').click();
        cy.get('.bm-item-list').should('be.visible');
        cy.get('#logout_sidebar_link').click();
    }

});