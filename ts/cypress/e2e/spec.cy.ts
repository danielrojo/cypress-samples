import 'cypress-wait-until';

xdescribe('template spec', () => {
  xit('passes', () => {
    cy.visit('https://example.cypress.io')
  })

  xit('should perform basic google search', () => {
    cy.visit('https://www.google.com/?gws_rd=ssl');
    cy.get('#W0wltc > .QS5gu').click();
    cy.get('#APjFqb').click();
    cy.get('#APjFqb').type('Adios');
    cy.get('form > div:nth-child(1)').click();
    cy.get('center:nth-child(1) > .gNO89b').click();
    cy.url().should('contains', 'https://www.google.com/search');
  });

  it('Performs operation: 45*6=270', () => {
    cy.visit('https://www.desmos.com/scientific?lang=es');
    cy.waitUntil(() => cy.get('.dcg-mq-mathspeak').contains('0'));

    cy.get('[dcg-command="4"]').click();
    cy.get('[dcg-command="5"]').click();
    cy.get('[dcg-command="*"]').click();
    cy.get('[dcg-command="6"]').click();
    cy.get('[dcg-command="enter"]').click();
    // check if result is 270
    cy.get('.dcg-mq-mathspeak').contains('270');
  });
})

xdescribe('template spec', () => {
  it('should work with a form', () => {
    // cy.visit('https://www.formsite.com/templates/order-form-templates/credit-card-order-form/');
    // cy.get('#imageTemplateContainer > img').click();  
    cy.visit('https://fs1.formsite.com/res/showFormEmbed?EParam=B6fiTn%2BRcO5Oi8C4iSTjsiapa7cvPnMb&138605154&EmbedId=138605154');
    cy.get('#RESULT_TextField-3').click();
    cy.get('#RESULT_TextField-3').type('1');
    cy.get('#RESULT_RadioButton-4').select('Radio-2');
    cy.get('#RESULT_RadioButton-5').select('Radio-0');
    // cy.get('.highlight > label').click();
    cy.get(':nth-child(1) > td > label').click()
    cy.get('.calculation_button').click();
    cy.get('span:nth-child(1)').contains('27');
  }
  )
})

xdescribe ('template spec', () => {

  let formaData = {
    nombre: 'daniel',
    apellido: 'Rojo',
    email: 'uno@cualquiera.com',
    telefono: '0123456789',
    password: 'password',
    confirmPassword: 'password'
  }

  function fillForm() {
    cy.get('#nombre').click();
    cy.get('#nombre').type(formaData.nombre);
    cy.get('#apellido').click();
    cy.get('#apellido').type(formaData.apellido);
    cy.get('#email').click();
    cy.get('#email').type(formaData.email);
    cy.get('#telefono').click();
    cy.get('#telefono').type(formaData.telefono);
    cy.get('#password').click();
    cy.get('#password').type(formaData.password);
    cy.get('#confirm-password').click();
    cy.get('#confirm-password').type(formaData.confirmPassword);
    cy.get('input:nth-child(25)').click();
  }

  function checkWrongInput(): void {
    cy.get('.success').should('have.length', 0);
    cy.get('.error').should('be.visible');
  }

  beforeEach(() => {
    formaData = {
      nombre: 'daniel',
      apellido: 'Rojo',
      email: 'uno@cualquiera.com',
      telefono: '0123456789',
      password: 'password',
      confirmPassword: 'password'
    }
    cy.visit('http://localhost:8080/formulario.html');
  })

  it('should work with a form', () => {
    fillForm();
    cy.get('.success').should('be.visible');
  }
  );

  it('wrong name', () => {
    formaData.nombre = 're';
    fillForm();
    checkWrongInput();
  });

  it('wrong lastName', () => {
    formaData.apellido = 're';
    fillForm();
    checkWrongInput();
  }
  );
});
