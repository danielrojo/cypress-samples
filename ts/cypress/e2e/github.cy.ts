xdescribe('GitHub', () => {
  xit('should be able to go to trending', () => {
    cy.visit('https://github.com/trending');
    cy.url().should('contains', 'https://github.com/trending');
    // svg elements with class octicon
    let svgs = cy.get('svg.octicon');
    // print the number of svg elements        

  });


  xit('links to github', () => {
    cy.visit('https://github.com/trending');
    cy.get('.Box-row').find("h2 > a").each((link) => {
      let urlToVisit = link.prop('href')
      cy.visit(urlToVisit)
    });
  });

  it('links to github topics', () => {
    cy.visit('https://github.com/topics');
    cy.get('.topic-box').find("a").each((link) => {
      let urlToVisit = link.prop('href')
      cy.visit(urlToVisit)
      // get content of the page and put it in a variable
      cy.get('body').then((body) => {
        // wrap the body as a jQuery object
        const $body = Cypress.$(body)
        // use jQuery functions to read whatever you want
        const someText = $body.find('a').text()
        // check if contains awesome
        if (someText.includes('awesome')) {
          cy.get('a .text-bold .wb-break-word').find('a').each((link) => {
            let urlToVisit = link.prop('href')
            // if urlToVisit doesnt contains /topics/ then visit it
            if (!urlToVisit.includes('/topics/')) {
              cy.visit(urlToVisit)
            }
          });
        }
      });
    });
  });
});