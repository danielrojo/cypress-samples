import { defineConfig } from "cypress";


export default defineConfig({
  projectId: 'ysxkve',
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
      require('cypress-terminal-report/src/installLogsPrinter')(on);
    },
  },
});
